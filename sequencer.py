import spi2B4B
import numpy as np
from matplotlib import pyplot as plt
import time

NUMBER_OF_CHANNELS = 16
MAX_EDGES_PER_CHANNEL = 8

FPGA_clk = 150000000 # Hz
FPGA_period = 1./FPGA_clk

FPGA_sub_period = FPGA_period/8.

pulse_data = {}
ch_is_inverted = [0] * NUMBER_OF_CHANNELS
ch_is_active   = [1] * NUMBER_OF_CHANNELS

cycle_period = 100e-9

device = 0


# register addresses
compilation_datecode_register = 8
compilation_timecode_register = 9
unique_id_register            = 10
temperature_register          = 11
TDC_status_registers          = list(range(12,12+4))
TDC_scaler_registers          = list(range(16,16+4))
FIFO_registers                = list(range(0,0+4))
seq_status_register           = 21
TDC_control_registers         = list(range(32,32+4)) # aka enable test pulser register (bit 0)
TDC_global_control_register   = 36
tdc_last_cycle_register       = 37
tdc_trig_rst_register         = 38 # self-clearing
fpga_reset_register           = 39 # self-clearing
seq_reset_register            = 58 # self-clearing
seq_config_register           = 59
seq_trigger_register          = 60 # self-clearing
last_cycle_register           = 61
invert_ch_register            = 62
enable_ch_register            = 63
sequencer_data_start_register = 64

def reboot_fpga():
    spi2B4B.write(device,fpga_reset_register,0x10)

def reset_fpga():
    spi2B4B.write(device,fpga_reset_register,0x1)

def read_tdc_scaler(ch):
    val = spi2B4B.read(device,TDC_scaler_registers[ch])
    scaler = val & 0x000FFFFF
    reftime = (val>>20) & 0x0FFF
    return (scaler,reftime)

def read_tdc_scaler_rate_hz(ch,**kwargs):
    
    time_reference = kwargs.get("time_reference","FPGA") # or "CPU"
    
    interval = kwargs.get("interval",0.1)
    scaler_a, reftime_a = read_tdc_scaler(ch)
    time.sleep(interval)
    scaler_b, reftime_b = read_tdc_scaler(ch)
    
    #max_internal_reftime = 2**12 * 2**16 * FPGA_period
    
    ref_interval = interval # if time_reference == "CPU"

    # if possible, use the FPGA generated reference time
    if (time_reference == "FPGA"):
        ref_interval_raw = reftime_b - reftime_a # measured in 16 bit overflows
        # i.e. 2**16*FPGA_period
        if ref_interval_raw < 0:
            ref_interval_raw += 2**12 # 12 bit counter
    
        ref_interval = ref_interval_raw* 2**16*FPGA_period
        
        #print("ref interval {}".format(ref_interval))
        
        # check if ref_interval is plausible
        # if it is too far away from the sleep interval, then an overflow must have happened
        while( abs(interval - ref_interval) > .1):
            ref_interval_raw += 2**12 # 12 bit counter
            ref_interval = ref_interval_raw* 2**16*FPGA_period
        
    scaler_difference = scaler_b -scaler_a
    if scaler_difference < 0:
        scaler_difference += 2**20 # 20 bit counter
    
    
    rate = scaler_difference/ref_interval
    
    # debug:
    if rate < 0:
        print("negative rate occured, debug:")
        print("scaler_a={} reftime_a={}".format(scaler_a,reftime_a))
        print("scaler_b={} reftime_b={}".format(scaler_b,reftime_b))
    
    return round(rate,1)

def enable_tdc():
    spi2B4B.set_bit(device,TDC_global_control_register,0,1)
    
def disable_tdc():
    spi2B4B.set_bit(device,TDC_global_control_register,0,0)
    
def reset_tdc():
    
    # disables TDC, empties all FIFOs
    disable_tdc()
    for ch in range(4):
        spi2B4B.set_bit(device,TDC_control_registers[ch],3,1)
        
    time.sleep(0.001) # wait a millisecond
    
    for ch in range(4):
        spi2B4B.set_bit(device,TDC_control_registers[ch],3,0)
    
    
def enable_tdc_chan(ch):
    spi2B4B.set_bit(device,TDC_control_registers[ch],0,1)
    
def disable_tdc_chan(ch):
    spi2B4B.set_bit(device,TDC_control_registers[ch],0,0)

def enable_test_pulser(ch):
    spi2B4B.set_bit(device,TDC_control_registers[ch],2,1)
    
def disable_test_pulser(ch):
    spi2B4B.set_bit(device,TDC_control_registers[ch],2,0)

    
def slurp_fifo(ch,**kwargs):
    block_size = kwargs.get("block_size",256)
    max_blocks = kwargs.get("max_blocks",100)
    
    block = []

    for i in range(max_blocks):
        this_block = spi2B4B.read_fifo_block(device,FIFO_registers[ch],4,block_size)
        block += this_block
        last_word = this_block[-1]
        fifo_empty    = (last_word>>(3*8+7)) & 0x1
        if fifo_empty:
            break
        
    return block  

def decode_block_to_raw_edges(block):

    # edge times are 1/f_clk * 1/16
    edge_times_raw  = []
    edge_types    = []
    #fine_times_r  = []
    #fine_times_f  = []

    edge_types_buf = []
    edge_times_raw_buf = []

    for word in block:

        fifo_err    = (word>>(3*8+7)) & 0x1
        if fifo_err:
            continue
        fifo_almost_full = (word>>(3*8+6)) & 0x1
        event_type  = (word>>(3*8+4)) & 0b0011 # (0=data, 1=epoch, 2,3=data_loss)
        ctr         = (word>>(3*8+1))   & 0b0111

        epoch = 0



        if event_type == 0: # data

            coarse_time = (word>>(2*8))   & 0xFF
            fine_word   = word & 0xFFFF

            hist0       = (word>>(3*8+0)) & 0x1


            #print("event_type : {}".format(event_type))

            my_edge_types, my_edge_fine_times = decode_fine_time(hist0,fine_word)

            for i in range(len(my_edge_types)):
                edge_types_buf  += [my_edge_types[i]]
                edge_times_raw_buf  += [my_edge_fine_times[i] + coarse_time*16]

                #if my_edge_types[i]: # rising edge
                #    fine_times_r += [my_edge_fine_times[i]]
                #else:
                #    fine_times_f += [my_edge_fine_times[i]]

        elif event_type == 1: # epoch

            epoch = word & 0xFFFFFF
            #print("epoch: {}".format(epoch))  
            #print(edge_types_buf)
            edge_types += edge_types_buf
            edge_times_raw += [ val + epoch*16*256 for val in edge_times_raw_buf]

            edge_types_buf = []
            edge_times_raw_buf = []
            
        elif event_type >= 2: # data loss
            #print("data loss event. (type {})".format(event_type))
            lost_events = word & 0xFFFFFF
            #print("data loss counter = {}".format(lost_events))
            # clear buffer, if there's data in there it means nothing
            edge_types_buf = []
            edge_times_raw_buf = []
            # if data loss occurs send a special edge type (-1)
            # to signal data loss and to prevent edges from before
            # and after the data loss from being paired
            edge_types     += [-1]
            edge_times_raw += [ lost_events ] # 


    #edge_times_raw   = np.array(edge_times_raw)
    #edge_types       = np.array(edge_types)
    #fine_times_r = np.array(fine_times_r)
    #fine_times_f = np.array(fine_times_f)
    return (edge_types,edge_times_raw)


def decode_block_to_pulses(block,**kwargs):
    
    # returns numpy
    
    polarity = kwargs.get("polarity",1)
    
    tick = FPGA_period/16
    
    edge_types, edge_times_raw = decode_block_to_raw_edges(block)
    
    widths           = []
    leading_edges    = []

    last_leading = -1

    for i in range(len(edge_times_raw)):
        my_time = edge_times_raw[i]
        my_type = edge_types[i]
        
        if my_type == -1: 
            # there has been a data loss due to fifo overflow,
            # throw away memory of previous leading edges
            # to not pair edges from before and after the data loss
            last_leading = -1
        
        if polarity != 1: # invert polarity if needed
            my_type = 1-my_type

        if last_leading >= 0:
            if my_type == 0: # falling edge
                my_width = my_time - last_leading
                #if my_width < 0:
                #    my_width += ovf_coarse_cycles *16
                widths += [my_width * tick]
                leading_edges += [last_leading * tick]

        if my_type == 1:
            last_leading = my_time
            
    return (np.array(leading_edges), np.array(widths))

def decode_block_to_box_diagrams(block):
    edg_type, edg_raw = decode_block_to_raw_edges(block)
    edg_type = np.array(edg_type)
    edg_raw  = np.array(edg_raw)

    fine_times_r = edg_raw[edg_type == 1] % 16
    fine_times_f = edg_raw[edg_type == 0] % 16
    
    return (fine_times_r,fine_times_f)
        
def empty_fifo():
    disable_test_pulser()
    disable_tdc()
    
    slurp_fifo()
    
    #block = []

    #for i in range(int(10000/512)):
    #    block = spi2B4B.read_fifo_block(device,0,4,512)

    #word = block[-1]
    #fifo_err    = (word>>(3*8+7)) & 0x1
    return
    


def logic_list_to_uint(a):
    val=0
    for i in range(len(a)):
        if a[i]:
            val += 1<<i
    return val

def spice_float(argument):
   
  if( isinstance(argument,str)):
   
    expr = argument
    if("p" in expr):
      expr = expr.replace("p","e-12")
    elif("n" in expr):
      expr = expr.replace("n","e-9")
    elif("u" in expr):
      expr = expr.replace("u","e-6")
    elif("m" in expr):
      expr = expr.replace("m","e-3")
    elif("k" in expr):
      expr = expr.replace("k","e3")
    elif("Meg" in expr):
      expr = expr.replace("Meg","e6")
    elif("M" in expr):
      expr = expr.replace("M","e6")
    elif("G" in expr):
      expr = expr.replace("G","e9")
    elif("T" in expr):
      expr = expr.replace("T","e12")
      
    try:
      number = float(expr)
    except:
      raise NameError("cannot convert \"{}\" to a reasonable number".format(argument))
  else:
    number = float(argument)
  
  return number



def reset_channels():
    global ch_is_inverted
    global ch_is_active
    
    for i in range(len(ch_is_inverted)):
        ch_is_inverted[i] = 0
        
    for i in range(len(ch_is_active)):
        ch_is_active[i]   = 0
        


def set_invert_ch(ch,val):
    global ch_is_inverted
    if val:
      ch_is_inverted[ch] = 1
    else:
      ch_is_inverted[ch] = 0
    
def get_invert_ch():
    return ch_is_inverted


def invert_ch(ch):
    set_invert_ch(ch,1)
    
def uninvert_ch(ch):
    set_invert_ch(ch,0)


def enable_ch(ch):
    ch_is_active[ch] = 1
    
def disable_ch(ch):
    ch_is_active[ch] = 0

def set_active_ch(ch,val):
    global ch_is_activeed
    if val:
      ch_is_active[ch] = 1
    else:
      ch_is_active[ch] = 0
    
def get_active_ch():
    return ch_is_active
    
def get_status():
    return spi2B4B.read(device,seq_status_register)

def get_seq_running():
    return bin(spi2B4B.read(device,seq_status_register))[2:3]

def calc_coarse_fine_time(t):
    coarse_time = int(t/FPGA_period)
    
    residual = t - (coarse_time*FPGA_period)
    
    fine_time = int(residual/FPGA_sub_period)
    
    return coarse_time, fine_time


def get_channels():
    return pulse_data.keys()

def clear_pulses():
    global pulse_data
    pulse_data = {}

def pulse(**kwargs):
    ch    = kwargs.get("ch",0)
    start = spice_float(kwargs.get("start",0.0))
    width = spice_float(kwargs.get("width",10e-9))
    stop  = spice_float(kwargs.get("stop",start+width))
    inv   = kwargs.get("inv",False)
    
    if stop <= start:
        raise NameError("pulse cannot have negative pulse width!")
    
    label = kwargs.get("label","")
    
    global pulse_data
    
    if not(ch in pulse_data.keys()):
      pulse_data[ch] = {'re'    : [],
                        'fe'    : [],
                        'label' : [],
                       }
    
    # new rising edge
    pulse_data[ch]['re']    += [start]
    # new falling edge
    pulse_data[ch]['fe']    += [stop]
    pulse_data[ch]['label'] += [label]
    
    
    
    
def build_trace_registers(ch):
    
    coarse_words = []
    fine_words   = []
    
    global pulse_data
    
    if ch in pulse_data.keys():
        rising_edges  = pulse_data[ch]['re'].copy()
        falling_edges = pulse_data[ch]['fe'].copy()
        
        
        edges_x      = rising_edges + falling_edges
        edges_y      = [1]*len(rising_edges) + [-1]*len(falling_edges)
        
        edges_x,edges_y = zip(*sorted(zip(edges_x,edges_y)))
        
        last_sum         = 0
        last_coarse_time = 0
        last_fine_time   = 0
        fine_word        = 0x00
        last_state       = 0
        open_word        = False
        
        for i in range(len(edges_x)):
            t = edges_x[i]
            coarse_time,fine_time = calc_coarse_fine_time(t)
            cur_sum = last_sum + edges_y[i]
            
            
            # finish the current fine word, init a new fine word
            if (coarse_time > last_coarse_time) and (i>0):
                if open_word == True:
                    coarse_words += [last_coarse_time]
                    fine_words   += [fine_word]
                open_word = False
                # store coarse/fine words
                fine_word = 0xFF*last_state
            
            # net rising edge
            if (last_sum == 0) and (cur_sum == 1):
                open_word = True
                fine_word |= (0xFF >> fine_time) & 0xFF
                last_state = 1
                
                    
                        
            # net falling edge
            elif (last_sum == 1) and (cur_sum == 0):
                open_word = True
                fine_word  &= (~(0xFF >> fine_time) )&0xFF
                last_state = 0
            
            
            
            
            last_sum         = cur_sum
            last_coarse_time = coarse_time
            last_fine_time   = fine_time
            
        # end of trace data, store the last open word

        if open_word == True:
            coarse_words += [coarse_time]
            fine_words   += [fine_word]
                
            
        
    
    return coarse_words, fine_words



def init_sequencer():
    
    global device
    # Open SPI device
    device = spi2B4B.open(freq=int(30e6))
    comm_test()
    clear_registers()
    
    
def read_identifier():
    my_string = ""
    block = spi2B4B.read_block(device,0,8)
    for j in range(7,-1,-1):
        word = block[j]
        for i in range(3,-1,-1):
            my_byte = (word>>(8*i)) & 0xFF
            if my_byte:
                my_string += chr(my_byte)
            
    return my_string
    
def comm_test():
#    test_words = [0x0, 0xFF, 0xAA, 0x55, 0x00]
#    for test_data in test_words:
#        test_data = 0xAA
#        spi2B4B.write(device,0,test_data)
#        answer = spi2B4B.read(device,0)
#        if answer != test_data:
#            raise NameError('SPI communication faulty, cannot read/write FPGA registers properly')
    ident_str = read_identifier()
    if "MUPPET" in ident_str:
        print(ident_str)
        print("SPI access okay")
    else:
        raise NameError('SPI communication faulty, cannot read/write FPGA registers properly')


# Sequencer configuration

def set_cycle_period(period):
    global cycle_period
    cycle_period = spice_float(period)
    spi2B4B.write(device,last_cycle_register,round(cycle_period/FPGA_period)-1)

def get_cycle_period():
    return cycle_period

def sequencer_reset():
    spi2B4B.write(device,seq_reset_register,1)

def sequencer_enable():
    spi2B4B.set_bit(device,seq_config_register,0,1)

def sequencer_disable():
    spi2B4B.set_bit(device,seq_config_register,0,0)

def seq_ext_trig_en(val):
    if val == 1:
        spi2B4B.set_bit(device,seq_config_register,4,1)
    elif val == 0:
        spi2B4B.set_bit(device,seq_config_register,4,0)
    else:
        raise NameError('Input parameter invalid, set 0 or 1')

def seq_free_running(val):
    if val == 1:
        spi2B4B.set_bit(device,seq_config_register,8,1)
    elif val == 0:
        spi2B4B.set_bit(device,seq_config_register,8,0)
    else:
        raise NameError('Input parameter invalid, set 0 or 1')

def seq_update_config():
    spi2B4B.write(device,seq_trigger_register,0x1)

def seq_arm_single_shot():
    spi2B4B.write(device,seq_trigger_register,0x10)

def seq_soft_trigger():
    spi2B4B.write(device,seq_trigger_register,0x100)


# Pulse generator channel(s) configuration

def configure_ppg_channels():
    # setting the invert register
    spi2B4B.write(device,invert_ch_register,logic_list_to_uint(get_invert_ch()))
    # setting the enable register
    spi2B4B.write(device,enable_ch_register,logic_list_to_uint(get_active_ch()))
    # Clear pulse registers
    clear_pulse_registers()
    # Write new pulse settings
    write_pulse_registers()
    
def clear_pulse_registers():
    spi2B4B.write_block(device,64,[0]*256)

def write_pulse_registers():
    
    clear_pulse_registers()
     
    for ch in get_channels():
    
        coarse_words, fine_words = build_trace_registers(ch)
        
        if len(coarse_words) > MAX_EDGES_PER_CHANNEL:
            raise NameError("sorry, you exceeded the possible number of pulse edges for channel {:d}".format(ch))

        for i in range(len(coarse_words)):
    
            #print("coarse cnt: {:010d} fine word: 0b{:08b}".format(
            #    coarse_words[i],fine_words[i]))
            
            spi2B4B.write(device, sequencer_data_start_register + ch*MAX_EDGES_PER_CHANNEL+i,coarse_words[i])
            my_fine_word = fine_words[i]
            if ch_is_inverted[ch]:
                # invert the data
                my_fine_word = (~my_fine_word)& 0xFF
            spi2B4B.write(device, sequencer_data_start_register + ch*MAX_EDGES_PER_CHANNEL+i+MAX_EDGES_PER_CHANNEL*NUMBER_OF_CHANNELS,my_fine_word)


# Old combinded configuration

def clear_registers():
    spi2B4B.write_block(device,0,[0]*512)
            
def program_sequencer():
    
    clear_registers()
    
    # setting the invert register
    spi2B4B.write(device,invert_ch_register,logic_list_to_uint(get_invert_ch()))
    
    # setting the enable register
    spi2B4B.write(device,enable_ch_register,logic_list_to_uint(get_active_ch()))
    
    # the "last cycle" register
    spi2B4B.write(device,last_cycle_register,round(cycle_period/FPGA_period)-1)
    
    for ch in get_channels():
    
        coarse_words, fine_words = build_trace_registers(ch)
        
        if len(coarse_words) > MAX_EDGES_PER_CHANNEL:
            raise NameError("sorry, you exceeded the possible number of pulse edges for channel {:d}".format(ch))

        for i in range(len(coarse_words)):
    
            #print("coarse cnt: {:010d} fine word: 0b{:08b}".format(
            #    coarse_words[i],fine_words[i]))
            
    
            spi2B4B.write(device, sequencer_data_start_register + ch*MAX_EDGES_PER_CHANNEL+i,coarse_words[i])
            my_fine_word = fine_words[i]
            if ch_is_inverted[ch]:
                # invert the data
                my_fine_word = (~my_fine_word)& 0xFF
            spi2B4B.write(device, sequencer_data_start_register + ch*MAX_EDGES_PER_CHANNEL+i+MAX_EDGES_PER_CHANNEL*NUMBER_OF_CHANNELS,my_fine_word)
    
    
    
def build_trace_preview_data(ch,x_min,x_max):
    xdata = []
    ydata = []
    
    vis_labels = []
    vis_labels_x = []
    
    global pulse_data
    
    if ch in pulse_data.keys():
        rising_edges  = pulse_data[ch]['re'].copy()
        falling_edges = pulse_data[ch]['fe'].copy()
        labels        = pulse_data[ch]['label'].copy()
        
        
        edges_x      = rising_edges + falling_edges
        edges_y      = [1]*len(rising_edges) + [-1]*len(falling_edges)
        edges_labels = labels + [""]*len(labels)
        
        edges_x,edges_y,edges_labels = zip(*sorted(zip(edges_x,edges_y,edges_labels)))
        
        last_sum    = 0
        for i in range(len(edges_x)):
            t = edges_x[i]
            cur_sum = last_sum + edges_y[i]
            label = edges_labels[i]
            
            
            # a start label
            if edges_y[i] == 1:
                if (t >= x_min) and (t < x_max):
                    if len(label)>0: ## if it is not dummy label
                        vis_labels += [label]
                        vis_labels_x += [t]            
            
            
            # net rising edge
            if (last_sum == 0) and (cur_sum == 1):
                if (t >= x_min) and (t < x_max):
                    # if nothing there yet, make entry
                    if len(xdata) == 0:
                        xdata += [x_min]
                        ydata += [0]
                    
                    xdata += [t]
                    ydata += [0]
                    xdata += [t]
                    ydata += [1]
                    
                        
            # net falling edge
            elif (last_sum == 1) and (cur_sum == 0):
                if (t >= x_min) and (t < x_max):
                    # if nothing there yet, make entry
                    if len(xdata) == 0:
                        xdata += [x_min]
                        ydata += [1]
                    
                    xdata += [t]
                    ydata += [1]
                    xdata += [t]
                    ydata += [0]
            
            
            
            
            last_sum = cur_sum
        
        
        
        # make sure you at least draw a 0 line if there are no pulses
        if len(xdata) == 0:
            xdata += [x_min]
            ydata += [0]
                
        # draw the line till the end of the plot        
        xdata += [x_max]
        ydata += [ydata[-1]]
                
            
        
    
    return xdata,ydata, vis_labels, vis_labels_x
    
    
def preview_sequence(**kwargs):
    
    labels     = kwargs.get("labels",True)
    xunit      = kwargs.get("xunit","ns")
    
    xmult      = 1
    
    if xunit == "ns":
        xmult = 1e9
    elif xunit == "us":
        xmult = 1e6
    elif xunit == "ms":    
        xmult = 1e3
    elif xunit == "s":
        xmult = 1
    elif xunit == "ps":  
        xmult = 1e12
        
        
        
    #x_range = x_max - x_min
    
    start = spice_float(kwargs.get("start",0.0))
    width = spice_float(kwargs.get("width",1000e-9))
    stop  = spice_float(kwargs.get("stop",start+width))
    
    if stop <= start:
        raise NameError("preview window cannot have negative width!")
    
    
    #plt.ylim([-0.1,1.1])
    
    #x_plot_range = x_range/zoom
    #pos_abs      = pos*(x_range-x_plot_range)
    #x_L = pos_abs
    #x_R = pos_abs + x_plot_range
    x_L = start
    x_R = stop
    
    
    plt.xlim([x_L*xmult,x_R*xmult])
    
    ch_inc = 0
    
    for ch in get_channels():

        xdata,ydata, vis_labels, vis_labels_x = build_trace_preview_data(ch,x_L,x_R)
        
        linestyle = "-"
        endis = ""
        if ch_is_inverted[ch]:
            ydata = 1-np.array(ydata)
            
        if not(ch_is_active[ch]):
            linestyle = ":"
            endis = " (disabled)"
        
        xdata = np.array(xdata)*xmult
        vis_labels_x = np.array(vis_labels_x)*xmult

        if labels:
            for i in range(len(vis_labels)):
                label_txt = vis_labels[i]
                label_x   = vis_labels_x[i]
                plt.text(label_x,0.5+ch_inc,label_txt,rotation=90)

        plt.plot(xdata,
                 np.array(ydata)*0.8+ch_inc,
                 linestyle,
                 label="CH{}{}".format(ch,endis))
        ch_inc += 1
    plt.grid(True)
    plt.legend()
    plt.xlabel("time ({})".format(xunit))
    plt.ylabel("logic level +n")
    plt.show()
    
    
    
def edge_detect(last_bit_in,fine_word,bits):
    edge_type = []
    edge_time = []
    
    xorvec = (last_bit_in<<(bits-1) |fine_word>>1) ^ fine_word
    
    for i in range(0,bits):
        # MSB first
        if (xorvec>>(bits-1-i)) & 0x1:
            edge_time += [i]
            edge_type += [(fine_word>>(bits-1-i)) & 0x1]
    # 1 = rising edge
    # 0 = falling edge
    return (edge_type, edge_time)


def decode_fine_time(last_bit_in, fine_word):
    
    edge_type = []
    edge_time = []
    
    fine_word_0 = (fine_word>>8) & 0xFF
    fine_word_1 = fine_word & 0xFF
    
    edge_detect_word_0 = (last_bit_in<<(8-1) |fine_word_0>>1) ^ fine_word_0
    
    xor01 = fine_word_0 ^ fine_word_1
    
    for i in range(0,8):
        # MSB first
        if (edge_detect_word_0>>(8-1-i)) & 0x1:
                                  # fine time correction from word_1 
            edge_time += [2*i   +   ((xor01>>(8-1-i)) & 0x1)   ]
                            # is rising edge?
            edge_type += [ (fine_word_0>>(8-1-i)) & 0x1 ]
    # 1 = rising edge
    # 0 = falling edge
    return (edge_type, edge_time)



def software_trigger(data,**kwargs):
    
    trig_chan  = kwargs.get("trig_chan",0)
    trig_win_L = kwargs.get("window_L",-1e-6)
    trig_win_R = kwargs.get("window_R",-1e-6)
    
    channels = list(data.keys())
    channels.sort()
    
    
    # prepare the output data structure
    triggered_data = {}
    
    index_mem = [0]*int(np.max(channels)+1)
    
    for ch in channels:
        triggered_data[ch] = {}
        triggered_data[ch]["le"]    = []
        triggered_data[ch]["width"] = []
    
        
        
    for trig_time in data[trig_chan]["le"]:
        win_L = trig_time + trig_win_L
        win_R = trig_time + trig_win_R
        #print("trig_time: {}".format(trig_time))
    
        for ch in channels:
            #print("  chan {}".format(ch))
            
            for i in range(index_mem[ch],len(data[ch]["le"])):
                leading_edge = data[ch]["le"][i]
                if leading_edge < win_L:
                    index_mem[ch] = i
                elif leading_edge < win_R:
                    #print("    lead - ref: {}".format(leading_edge-trig_time))
                    triggered_data[ch]["le"]    += [leading_edge - trig_time]
                    triggered_data[ch]["width"] += [data[ch]["width"][i]]
                else:
                    break
                    
    return triggered_data
