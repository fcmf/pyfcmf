#######################
pyFCMF
#######################

The Flexible Control and Measurement Framework (in python)

Introduction
=============

The pyFCMF framework developed by the department Experiment Electronics (EEL) at GSI
is an attempt to make state of the art **high-speed measurement and control
hardware** implemented on **FPGAs** accessible to everybody without the need
for expensive and complex readout hardware or software. Both, FPGA
implementations and the control and readout software are fully **open source**,
enabling motivated users to customize the system perfectly for their needs. 

We are currently using a **RaspberryPi(4)** single board computer
connected to the FPGA via one of its hardware SPI interfaces to transfer all
data to and from the FPGA. The user can directly work on the RaspberryPi or
connect via ssh or vnc or run a jupyter notebook server and operate the system
via the jupyter notebooks.

The toolbox of FPGA implementations is supposed to grow over time. Currently,
the set of implementations comprises an implementation of a very flexible
**pulse-pattern-generator (PPG) with 833ps granularity (1.2GS/s)** and a
**time-to-digital converter (TDC) system with a precision of 170ps RMS**. The
**MUPPET1 FPGA module**, also developed by GSI EEL, serves as a demonstration
platform. On the MUPPET1 module, up to 16 PPG channels plus 4-8 TDC channels
can be implemented.

This repository contains the *spi2B4B.py* python library for RaspberryPi-OS, enabling
the user to read/write the FPGA registers as well as to read the FPGA data
buffers via the SPI interface. The *sequencer.py* python library contains
high-level functions (an API) for the configuration, operation and readout of the PPG and TDC
implementations on the MUPPET1 module. These high-level functions are
showcased in the various demo jupyter notebooks (*.ipynb files).


Hardware implementations
==========================

In this section, the control and status register definitions of each gateware
design in the framework are listed:

MUPPET1_PPG_TDC
----------------

The MUPPET1 module is a multi-purpose FPGA module developed at GSI EEL. It
features a XILINX ARTIX7 FPGA and a multitude of I/O options with drivers and
receivers for common signaling standards like LVTTL/CMOS, NIM as well as
differential LVDS signals.

The MUPPET1_PPG_TDC design implements an 8CH/16CH pulse-pattern-generator
(PPG) and a 4CH 170ps sampling TDC system.

.. list-table:: Example table
   :widths: 25 25 50
   :header-rows: 1

   * - First header
     - Second header
     - Third header
   * - Some text
     - Some text
     - | A list:
       | [1] asdasd
       | [0] asd asd
   * - Second row
     - Second Row2
     - Second Row 3 sdlkfjsdlf s sldkjfsdlkf sdlfkjsdlkfj sdlfjk 
   * - ...
     - ...
     - ...
