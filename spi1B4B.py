# Python functions for SPI communication
# between Raspberry Pi (master) and KILOM module (slave)
# Henning Heggen, EE, GSI
# h.heggen@gsi.de
# Version 0.1

#!/usr/bin/env python3

import spidev
import struct

# Open SPI connection
def open():
    try:
        spi_device = spidev.SpiDev()
        spi_device.open(1,0)
        spi_device.max_speed_hz = 20000000
        spi_device.mode = 0
    except Exception as e:
        print("Failed to open SPI connection")
        print(e.message, e.args)
        return
    return spi_device


# Register write function accepting integers as arguments
def write(device, addr, data):
    msg = [0x00,0x00,0x00,0x00,0x00]
    msg[0] = 0b10000000 | (addr & 0b01111111)
    data = struct.unpack('4B',struct.pack('>I',data))
    msg[1]=data[0]
    msg[2]=data[1]
    msg[3]=data[2]
    msg[4]=data[3]
    r = device.xfer2(msg)
    return int.from_bytes(r[1:], signed=False, byteorder='big')

# Register read function accepting integers as arguments
def read(device, addr):
    msg = [0x00,0x00,0x00,0x00,0x00]
    msg[0] = addr & 0b01111111
    r = device.xfer2(msg)
    return int.from_bytes(r[1:], signed=False, byteorder='big')

def read_block(device, addr, n):
    msg = [0x00]*(4*n+1)
    msg[0] = addr & 0b01111111
    r = device.xfer2(msg)
    bytes_arr = []
    for i in range(n):
        bytes_arr += [int.from_bytes(r[1+i*4:1+i*4+4], signed=False, byteorder='big')]
    return bytes_arr

