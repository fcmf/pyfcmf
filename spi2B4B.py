# Python functions for SPI communication
# between Raspberry Pi (master) and KILOM module (slave)
# Henning Heggen, EE, GSI
# h.heggen@gsi.de
# Version 0.1

#!/usr/bin/env python3

import spidev
import struct

# Open SPI connection
def open(**kwargs):
    try:
        freq=kwargs.get("freq",30000000)
        spi_device = spidev.SpiDev()
        spi_device.open(1,0)
        spi_device.max_speed_hz = freq
        spi_device.mode = 0
    except Exception as e:
        print("Failed to open SPI connection")
        print(e.message, e.args)
        return
    return spi_device


# Register write function accepting integers as arguments
def write(device, addr, data):
    msg = [0x00]*6
    msg[0] = (addr & 0xFF0) >> 4
    msg[1] = (addr & 0xF) << 4 | 0b0100
    data = struct.unpack('4B',struct.pack('>I',data))
    msg[2] = data[0]
    msg[3] = data[1]
    msg[4] = data[2]
    msg[5] = data[3]
    r = device.xfer(msg)
    return int.from_bytes(r[2:], signed=False, byteorder='big')

def write_block(device, addr, data_list):
    msg = []
    answer = []
    # first two bytes are address and mode
    msg += [ (addr & 0xFF0) >> 4 ]
    msg += [ (addr & 0xF) << 4 | 0b0100 ]
    # the following bytes are data, each register has 4 bytes
    for data in data_list:
      data_bytes = struct.unpack('4B',struct.pack('>I',data))
      for byte in data_bytes:
        msg += [byte]
    # transfer message
    r = device.xfer(msg)
    # regroup byte groups into 32 bit integers
    for i in range(int(len(r)/4)):
      answer += [int.from_bytes(r[i*4+2:(i+1)*4+2], signed=False, byteorder='big')]
    return answer

# Register read function accepting integers as arguments
def read(device, addr):
    msg = [0x00]*6
    msg[0] = (addr & 0xFF0) >> 4
    msg[1] = (addr & 0xF) << 4 | 0b0000
    r = device.xfer(msg)
    return int.from_bytes(r[2:], signed=False, byteorder='big')

def read_fifo(device, addr, n_bytes):
    msg = [0x00]*(2+n_bytes)
    msg[0] = (addr & 0xFF0) >> 4
    msg[1] = (addr & 0xF) << 4 | 0b1000
    r = device.xfer(msg)
    return int.from_bytes(r[2:], signed=False, byteorder='big')

def read_fifo_block(device, addr, n_bytes, n_words):
    msg = [0x00]*(2+n_bytes*n_words)
    msg[0] = (addr & 0xFF0) >> 4
    msg[1] = (addr & 0xF) << 4 | 0b1000
    r = device.xfer2(msg)
    words_arr = []
    for i in range(n_words):
        words_arr += [int.from_bytes(r[2+i*n_bytes:2+(i+1)*n_bytes], signed=False, byteorder='big')]
    return words_arr

def read_block(device, addr, n):
    msg = [0x00]*(4*n+2)
    msg[0] = (addr & 0xFF0) >> 4
    msg[1] = (addr & 0xF) << 4 | 0b0000
    r = device.xfer2(msg)
    bytes_arr = []
    for i in range(n):
        bytes_arr += [int.from_bytes(r[2+i*4:2+i*4+4], signed=False, byteorder='big')]
    return bytes_arr


def get_bit(device,addr,bit):
    val = read(device,addr)
    return (val>>bit) & 0x1
    
def set_bit(device,addr,bit,value):
    mem = read(device,addr)
    mem = mem & ((~(1<<bit))& 0xFFFFFFFF) | ((value & 0x1)<<bit)
    write(device,addr,mem)
